import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Photo } from '../Interface/Photo';
@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  URI = 'http://localhost:4000/api/photos';
  constructor(private http: HttpClient) { }

  createPhoto(title: string, description: string, photo: File){
    const fd = new FormData()
    fd.append('title', title);
    fd.append('description', description);
    fd.append('image', photo);
    return this.http.post(this.URI, fd);
  }

  getPhotos(){
    return this.http.get<Photo[]>(this.URI);
  }

  // Cuando uso este metodo, le pasamos un id. El id que capturo de cada tarjeta. Cuando tenga el id, se lo doy al backend.
  // Una vez, el backend me rotorna los datos, lo tengo que reutilizar (daba error this.id porque es id (al ser el id pasado a funcion))
  getPhoto(id: string){
    return this.http.get<Photo>(`${this.URI}/${id}`);
  }
  
  deleteAllPhotos(){
    return this.http.delete<Photo[]>(this.URI);
  }
  
  deletePhoto(id: string){
    return this.http.delete(`${this.URI}/${id}`);
  }

  updatePhoto(id: string, title: string, description: string) {
    return this.http.put(`${this.URI}/${id}`, {title, description});
  }

}
