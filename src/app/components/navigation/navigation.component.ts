import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../../services/photo.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private photoService: PhotoService, private router: Router) { }

  ngOnInit(): void {
  }

  deleteAllPhotos() {
    this.photoService.deleteAllPhotos()
      .subscribe(res => {
        console.log(res)
        this.router.navigate(['/photos']);
      })
  }

}
