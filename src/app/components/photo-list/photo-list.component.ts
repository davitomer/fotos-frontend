import { Component, OnInit } from '@angular/core';
import {PhotoService} from '../../services/photo.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})
export class PhotoListComponent implements OnInit {

  photos = [];
  photo = File;

  constructor(private photoService: PhotoService, private router: Router) { }

  ngOnInit() {
    this.photoService.getPhotos()
    .subscribe(
      res => this.photos = res,
      err => console.log(err)  
    )
  }

  selectedCard(id: string){
    // console.log(id);  
    this.router.navigate(['/photos', id]);
  }


}
