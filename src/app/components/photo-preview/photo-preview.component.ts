import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../../services/photo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Photo } from '../../Interface/Photo';
@Component({
  selector: 'app-photo-preview',
  templateUrl: './photo-preview.component.html',
  styleUrls: ['./photo-preview.component.css']
})
export class PhotoPreviewComponent implements OnInit {
  id: string;
  photo: Photo;
  constructor(private router: Router, private activateRouter: ActivatedRoute, private photoService: PhotoService) { }

  ngOnInit() {
    this.activateRouter.params.subscribe(
      params => {
        // console.log(params['id'])  
        this.id = params['id']; 
        this.photoService.getPhoto(this.id).subscribe(
          res => this.photo = res,
          err => console.log(err)
        )
      })
  }

  deletePhoto(id: string) {
    this.photoService.deletePhoto(id)
      .subscribe(res => {
        console.log(res)
        this.router.navigate(['/photos']);
      })
  }

  updatePhoto(title: HTMLInputElement, description: HTMLInputElement): boolean {
    this.photoService.updatePhoto(this.photo._id, title.value, description.value)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(['/photos']);
      });
    return false;
  }


}
